from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TasksForm
from tasks.models import Task

# Create your views here.


@login_required
def task_create(request):
    if request.method == "POST":
        form = TasksForm(request.POST)
        if form.is_valid():
            create = form.save()
            create.owner = request.user
            create.save()
            return redirect("list_projects")
    else:
        form = TasksForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def tasks_user(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": task,
    }
    return render(request, "tasks/list.html", context)
