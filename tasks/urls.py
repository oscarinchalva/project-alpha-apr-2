from django.urls import path
from tasks.views import task_create, tasks_user

urlpatterns = [
    path("create/", task_create, name="create_task"),
    path("mine/", tasks_user, name="show_my_tasks"),
]
