from django.urls import path
from accounts.views import userlogin, userlogout, usersignup

urlpatterns = [
    path("login/", userlogin, name="login"),
    path("logout/", userlogout, name="logout"),
    path("signup/", usersignup, name="signup"),
]
